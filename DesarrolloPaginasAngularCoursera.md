# DESARROLLO DE PAGINAS CON ANGULAR 

## SEMANA 1
### LECCION 1 INSTALACION DE HERRAMIENTAS Y PRIMERA APLICACION ANGULAR
#### VIDEO INSTALACION DE HERRAMIENTAS PARA ANGULAR
**NVM** Node Version Manager Gesto de versiones de Nodejs es una utilidad para manejar versiones instaladas de nodejs en una misma pc  
https://github.com/coreybutler/nvm-windows/releases descarga archivo .zip y ejecutar instalador  
**nvm --version** consultar version instalada  
**nvm list** listar versiones instaladas, si hay mas de una version, el asterisco indica la version activa  
**nvm install latest** instalar ultima version de nodejs  
**nvm use X.X.X** activar la version de nodejs deseada  
**node --version** verificar version de nodejs activa  
**npm -v** verificar version NPM Node Package Manager Gestor de paquetes de nodejs. Si se instala nodejs desde nvm automaticamente se instalara el npm  
REPL interprete interactivo de nodejs  
**TYPESCRIPT** nuevas funcionalidades de javascript haciendolo mas productivo  
**npm install -g typescript** instalara de manera global typescript permitiendo su uso en cualquier aplicacion de node que lo requiera  
**ANGULAR CLI** es una interfaz de uso para desarrolladores para usar angular  
**npm install -g @angular/cli** instalara de manera global angular cli  
**ng --version** consulta version angular cli instalada  
**ng help** muestra ayuda para usar comandos en angular cli  
**ng comando --help** nos muestra ayuda puntual sobre el comando deseado

##### COMANDOS CMD DE INTERES
**CTRL + C** CORTA PROCESOS DE LA CMD  
**cls** limpia la cmd

#### VIDEO INSTALACION DE HERRAMIENTAS GIT
Crear repositorio en **https://bitbucket.org/**  
Descargar Git **https://git-scm.com/downloads** e instalar  
Abrir Git Bash para crear una llave SSH  
**ssh-keygen** comando para crear llave ssh poner nombre **id_rsa**  
**cd ~(alt126)** nos lleva a carpeta de usuario  
**explorer .** abre la carpeta  
**id_rsa.pub  id_rsa** copìar estos archivos a la carpeta **.ssh**  
abrimos el archivo **id_rsa.pub** en un editor y copiamos el codigo generado  
ingresamos a **https://bitbucket.org/account/settings/ssh-keys/** creamos una nueva llave pegamos el codigo copiado anteriormente y guardamos con un nombre  
clonamos el repositorio desde **Git Bash** ejemplo  
**cd ~/Desktop/angularcoursera/** carpeta deseada para clonar el repositorio  
copiamos la direccion desde el repositorio deseado  
**git clone git@bitbucket.org:Usuario/nombreRepositorio.git**  

##### COMANDOS DE INTERES EN CMD DE GIT
**git status** muestra estado del repositorio  
**git add .** trackea los nuevos cambios  
**git commit -m "mensaje para guardar"** guarda los cambios realizados  
**git push** sube al servidor los cambios realizados  
**git pull** jala desde el servidor los cambios realizados  
#### VIDEO PRIMERA APLICACION BASICA ANGULAR

### LECCION 2 TEMPLATES E INTEGRACION CON BOOTSTRAP
### LECCION 3 MANEJO DE COMPONENTES EN ANGULAR
### LECCION 4 INTRODUCCION A TYPESCRIPT

## SEMANA 2
## SEMANA 3
## SEMANA 4